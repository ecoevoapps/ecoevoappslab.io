---
# Display name
title: "Alejandra Martínez Blancas"

# Username (this should match the folder name and the name on publications)
authors:
- "alejandra-martinez-blancas"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns:

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Species coexistence
- Grassland conservation
- Dogs
- Food!

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: "mailto:test@example.org"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/alemtz_b
- icon: globe # personal website
  icon-pack: fas
  link: https://alemtzb1.wixsite.com/website
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=mdfKEK8AAAAJ&hl=en&oi=ao
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
weight: 9
---

Alejandra is a PhD student at Universidad Nacional Autónoma de México (UNAM) where she studies the effects of hydric stress on species coexistence in a semiarid grassland. She discovered her love of nature as a little girl going on walks with her grandmother. This love of nature only grew stronger after each field trip from elementary school up to her first years at the university. Nowadays she focuses on understanding nature using statistics and mathematical models.

Alejandra is also passionate about grassland conservation. She also enjoys live music (especially rock), reading, cooking, eating all kinds of food, walking with her dog, and of course hiking and exploring nature.
