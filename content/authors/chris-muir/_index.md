---
# Display name
title: "Chris Muir"

# Username (this should match the folder name and the name on publications)
authors:
- "chris-muir"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns:

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
# -
# -
# -
# -

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:cdmuir@hawaii.edu"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/thegoodphyte
# - icon: globe # personal website
#   icon-pack: fas
#   link: URL
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
- icon: github
  icon_pack: fab
  link: https://github.com/cdmuir
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- App contributors
weight: 0

draft: true
---

Chris Muir grew up in Virginia (USA) and did his PhD in Evolutionary Biology at Indiana University. He was a postdoctoral researcher at the University of British Columbia in Vancouver, Canada and worked as a biostatistician at Poisson Consulting and Novozymes. Chris currently is an Assistant Professor in the School of Life Sciences and the University of Hawai’i at Mānoa. He is Secretary of the Division of Botany for the Society for Integrative and Comparative Biology.

Chris is an evolutionary ecophysiologist interested in how and why plants adapt to different environments. He has worked with wild relatives of tomato and monkeyflowers, but also uses mathematical models and phylogenetic comparative analyses. He is also interested in developing new computational tools for plant ecophysiologists to make complex modelling and data analysis easier.
