---
# Display name
title: "Damla Cinoglu"

# Username (this should match the folder name and the name on publications)
authors:
- "damla-cinoglu"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns:

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Plant species coexistence
- Tropical forest succession
- Knitting
- Cats

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: "mailto:test@example.org"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/damlacinoglu1
- icon: globe # personal website
  icon-pack: fas
  link: https://damlacinoglu.github.io
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
weight: 8
---

Damla graduated from the University of Virginia with a B.S. in Economics and Environmental Sciences. She is currently a PhD candidate at the Farrior Lab in EEB at UT Austin. She is primarily interested in tropical forest dynamics and succession, functional and demographic tradeoffs, and species coexistence.

Damla enjoys all things nature, from going on hikes to scuba diving on the Mediterranean coast of Turkey. She is also very crafty and loves knitting and crocheting. She even knit a forest dynamics themed sweater once!

For more about Damla, check out [her website](https://damlacinoglu.github.io).
