---
# Display name
title: "Rosa M. McGuire"

# Username (this should match the folder name and the name on publications)
authors:
- "rosa-mcguire"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns: She/Her/Hers

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Climate change and species interactions
- Thermal biology
- Insect ecology
- Baking

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:rosamcguire@fastmail.com"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/mcguire_rm
# - icon: globe
#   icon-pack: fas
#   link: URL
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
- Instructors
weight: 3
---

Rosa McGuire became interested in ecology while growing up in Peru. During her junior year as an undergraduate she became passionate about theoretical ecology. Her current research focuses on temperature effects on life history traits and how climate change will affect species interactions using both experimental and theoretical approaches. She is particularly interested in parasitoid-host interactions.
