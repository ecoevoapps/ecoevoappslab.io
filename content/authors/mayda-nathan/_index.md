---
# Display name
title: "Mayda Nathan"

# Username (this should match the folder name and the name on publications)
authors:
- "mayda-nathan"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns:

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
# -
# -
# -
# -

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: "mailto:test@example.org"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/maydanathan
# - icon: globe
#   icon-pack: fas
#   link: URL
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- App contributors
- Instructors
weight: 0
---

Mayda recently completed her doctorate studying insect ecology at the University of Maryland, where she also discovered a love of teaching and R. She saw first-hand how students could gain a deeper understanding of (and love for) ecology by using simple mathematical models to make predictions, test hypotheses, and conceptually link diverse ecological phenomena.
