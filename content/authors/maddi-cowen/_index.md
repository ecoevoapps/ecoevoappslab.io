---
# Display name
title: "Maddi Cowen"

# Username (this should match the folder name and the name on publications)
authors:
- "maddi-cowen"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns: She/Her/Hers

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Birdwatching
- Cooking and baking
- Painting
- Reading and writing fiction

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:mcowen@g.ucla.edu"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/maddicowen
# - icon: globe # personal website
#   icon-pack: fas
#   link: URL
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
weight: 5
---

Maddi is a PhD student at UCLA, working on understanding and predicting bird species coexistence using a mix of theoretical models and observational studies. She cares deeply about teaching and mentoring students and making science more just.

Maddi grew up in Miami, Florida, where she became enamored with birds and nature and concerned about climate change.  She decided to pursue ecology after hanging out with lots of California scrub-jays at Pomona college.
