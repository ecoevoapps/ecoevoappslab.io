---
# Display name
title: "Gaurav Kandlikar"

# Username (this should match the folder name and the name on publications)
authors:
- "gaurav-kandlikar"

# Is this the primary user of the site?
superuser: true

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns: He/Him/His

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Species coexistence
- Plant diversity
- Running

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:gaurav.kandlikar@gmail.com"
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gauravsk_
- icon: globe # personal website
  icon-pack: fas
  link: https://gauravsk.gitlab.io
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
weight: 1
---

Gaurav Kandlikar is a postdoc at the University of Missouri, where his research focuses on how soil microorganisms and abiotic heterogeneity shape plant species coexistence. He became fascinated with mathematical ecology during the final year of his undergraduate, when he realized that he was learning how the same mathematical framework can be used to study predator-prey models in an ecology class (functional responses with handling time), and enzyme kinetics in a biochemistry class (Michelis-Menten dynamics).

Gaurav's love for ecology has roots in his childhood in southern India, which he spent scrambling up trees and watching lots of Animal Planet. He's always happy to talk about ecological models, diversity in EEB, pedagogy, running, and cooking.

For more about Gaurav, check out [his website](https://gauravsk.gitlab.io).
