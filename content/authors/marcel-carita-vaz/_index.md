---
# Display name
title: "Marcel Caritá Vaz"

# Username (this should match the folder name and the name on publications)
authors:
- "marcel-carita-vaz"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Pronouns
pronouns: He/Him/His

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)
bio:

# List each interest with a dash
interests:
- Forest ecology
- Plant evolution
- Functional diversity
- Inclusive science

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:marcelcvaz@gmail.com"
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/USERNAME
- icon: globe # personal website
  icon-pack: fas
  link: https://sites.google.com/g.ucla.edu/amazon-tree
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?user=PERSON-ID
# - icon: github
#   icon_pack: fab
#   link: https://github.com/USERNAME
# - icon: gitlab
#   icon_pack: fab
#   link: https://gitlab.com/USERNAME

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers and translators
- Instructors
weight: 6
---

Marcel Vaz became passionate for the Amazon forest after a field season during his MSc course, back in 2009. Shortly after, he started learning R and became completely addicted. He then had the opportunity to use R to teach Ecology and everything came together. Today his work is a product of the confluence of these three passions.
