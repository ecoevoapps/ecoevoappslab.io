---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Title and subtitle
title: "Meet the team"
subtitle: ""

# Order that this section appears on the page.
weight: 10

design:
  columns: "1"
  spacing:
    padding: ["40px", "0", "20px", "0"] # top, right, bottom, left
---

This project was initiated by a group of ecology and evolutionary biology graduate students who started writing these apps to help understand the models for ourselves, and as teaching tools for our students. None of us are experts in R, Shiny, web development, or even the models themselves, but we've learned a lot (and had a lot of fun) along the way. We are happy to share what we've learned, and eagerly welcome contributions from anyone who wants to [contribute](/contribute) to this effort! **We actively welcome people of all backgrounds to join us.**

All contributors must adhere to our [Contributors Code of Conduct](/code-of-conduct).
