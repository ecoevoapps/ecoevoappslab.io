---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: people

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Title and subtitle
title: ""
subtitle: ""

# Order that this section appears on the page.
weight: 20

content:
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups:
  - Developers and translators
  - Instructors
  - App contributors
  - App reviewers

design:
  # Show user's social networking links? (true/false)
  show_social: false
  # Show user's interests? (true/false)
  show_interests: false
  # Show user's role?
  show_role: true
  # Show user's organizations/affiliations?
  show_organizations: false
  spacing:
    padding: ["20px", "0", "20px", "0"]
---
