---
title: Seed size/number trade-off
summary: A model to explore the life history trade-off between offspring size and offspring number
authors:
- Gaurav Kandlikar
tags:
- Population ecology
date: 2021-05-13T22:15:11-07:00
show_date: false
weight: 100

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the allocation trade-off model from [Smith and Fretwell (1974)](https://www.journals.uchicago.edu/doi/10.1086/282929). This model explores the fundamental life history trade-off from the perspective of a tree that can invest a set amount of resources into making seeds of varying sizes. It can make many small seeds with low survival rate per seed, or fewer large seeds that are more likely to survive.

**Launch app in**:
[English](https://ecoevoapps.shinyapps.io/smith_fretwell_app/)
