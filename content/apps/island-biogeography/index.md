---
title: Island biogeography
summary: MacArthur and Wilson's model of island biogeography
authors:
- Marcel Caritá Vaz
tags:
- Landscape ecology
date: 2020-07-19T22:15:47-07:00
show_date: false
weight: 90

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics of the classic island biogeography framework developed by [MacArthur and Wilson](https://en.wikipedia.org/wiki/Insular_biogeography). Users can set the distance between mainland and island, island size, and species richness of the mainland.  

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/island_biogeography_ch/) |
[Español](https://ecoevoapps.shinyapps.io/island_biogeography_es/) |
[English](https://ecoevoapps.shinyapps.io/island_biogeography/) |
[português](https://ecoevoapps.shinyapps.io/island_biogeography_pt/) |
[Turkish](https://ecoevoapps.shinyapps.io/island_biogeography_tk/)
