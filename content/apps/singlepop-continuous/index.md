---
title: Single population growth in continuous time
summary: Exponential or logistic population growth in continuous time
authors:
- Gaurav Kandlikar
tags:
- Population ecology
date: 2020-07-19T22:14:56-07:00
show_date: false
weight: 10

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics of a single population growing in continuous time. The growth may be exponential (density independent), or logistic (density dependent) until a user-determined carrying capacity. If population growth is logistic, the app also allows there to be a time-lag in the density dependence.

**Launch app in**: 
[中文](https://ecoevoapps.shinyapps.io/population_growth_ch/) |
[Español](https://ecoevoapps.shinyapps.io/population_growth_es/) |
[English](https://ecoevoapps.shinyapps.io/population_growth/) |
[português](https://ecoevoapps.shinyapps.io/population_growth_pt) |
[Turkish](https://ecoevoapps.shinyapps.io/population_growth_tr/)
