---
title: Single population growth in discrete time
summary: Exponential or logistic population growth in discrete time
authors:
- Gaurav Kandlikar
- Alejandra Martínez Blancas
- Xinyi Yan
tags:
- Population ecology
date: 2020-07-19T22:15:01-07:00
show_date: false
weight: 20

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics of a single population growing in discrete time (e.g. annually). The growth may be exponential (density independent), or logistic (density dependent) until a user-determined carrying capacity. The app implements three different models of density-dependent discrete-time growth, including [Beverton-Holt](https://en.wikipedia.org/wiki/Beverton-Holt_model) and [Ricker](https://en.wikipedia.org/wiki/Ricker_model) models.

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/population_growth_discrete_ch/) |
[Español](https://ecoevoapps.shinyapps.io/population_growth_discrete_es/) |
[English](https://ecoevoapps.shinyapps.io/population_growth_discrete/) |
[português](https://ecoevoapps.shinyapps.io/population_growth_discrete_pt) |
[Turkish](https://ecoevoapps.shinyapps.io/population_growth_discrete_tr/)
