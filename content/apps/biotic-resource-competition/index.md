---
title: Biotic resource competition
summary: Interactions between consumers (predators) that feed on the same biotic resource (prey)
authors:
- Maddi Cowen
- Rosa McGuire
tags:
- Species interactions
date: 2020-07-19T22:15:32-07:00
show_date: false
weight: 70

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics between two consumers (e.g. predators or herbivores) that both consume the same biotic resource. The prey may be modeled as experiencing exponential or logistic growth, and the predators may be modeled as having a Type 1 or Type 2 functional response.

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/biotic_resource_competition_ch/) |
[Español](https://ecoevoapps.shinyapps.io/biotic_resource_competition_es/) |
[English](https://ecoevoapps.shinyapps.io/biotic_resource_competition/) |
[português](https://ecoevoapps.shinyapps.io/biotic_resource_competition_pt) |
[Turkish](https://ecoevoapps.shinyapps.io/biotic_resource_competition_tr/)
