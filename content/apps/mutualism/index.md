---
title: Mutualism
summary: Direct mutualism with saturating benefits
authors:
- Kenji Hayashi
tags:
- Species interactions
date: 2022-10-21
show_date: false
weight: 35

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app explores a model of direct mutualism between two species with saturating benefits, as described by [Holland (2012)](https://www.nature.com/scitable/knowledge/library/population-dynamics-of-mutualism-61656069/).

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/mutualism_ch/) |
[Español](https://ecoevoapps.shinyapps.io/mutualism_es/) |
[English](https://ecoevoapps.shinyapps.io/mutualism/) |
[português](https://ecoevoapps.shinyapps.io/mutualism_pt) |
[Turkish](https://ecoevoapps.shinyapps.io/mutualism_tr/)
