---
title: Predator-prey dynamics
summary: Dynamics of a predator  and its prey
authors:
- Maddi Cowen
- Rosa McGuire
tags:
- Species interactions
date: 2020-07-19T22:15:41-07:00
show_date: false
weight: 50

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app implements a range of models that simulate the dynamics of a predator (or hebivore) and its prey. Users can explore a range of options, including prey that grow exponentially or logistically, and predators that have type 1 or type 2 functional responses (i.e. Lotka-Volterra predator-prey dynamics, Rosenzweig-MacArthur dynamics, etc.).

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/predator_prey_dynamics_ch/) |
[Español](https://ecoevoapps.shinyapps.io/predator_prey_dynamics_es/) |
[English](https://ecoevoapps.shinyapps.io/predator_prey_dynamics/) |
[português](https://ecoevoapps.shinyapps.io/predator_prey_dynamics_pt/) |
[Turkish](https://ecoevoapps.shinyapps.io/predator_prey_dynamics_tr/)
