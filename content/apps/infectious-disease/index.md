---
title: Infectious disease
summary: Compartment models of disease dynamics
authors:
- Maddi Cowen
tags:
- Species interactions
date: 2020-07-19T22:16:15-07:00
show_date: false
weight: 80

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics of a class of models that follow an infectious disease spreading through a population that consists of individuals who are susceptible, infectious, recovered, and in some cases immune ("[compartment models](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology)").

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/sir_disease_dynamics_ch/) | [Español](https://ecoevoapps.shinyapps.io/sir_disease_dynamics_es/) | [English](https://ecoevoapps.shinyapps.io/sir_disease_dynamics/) | [português](https://ecoevoapps.shinyapps.io/sir_disease_dynamics_pt/) |  [Turkish](https://ecoevoapps.shinyapps.io/sir_disease_dynamics_tr/) 

