---
title: Lotka-Volterra competition
summary: Classic two-species competition model
authors:
- Kenji Hayashi
tags:
- Species interactions
date: 2020-07-19T22:15:19-07:00
show_date: false
weight: 40

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app allows users to simulate the dynamics of the classic two-species [Lotka-Volterra competition](https://en.wikipedia.org/wiki/Competitive_Lotka%E2%80%93Volterra_equations) model.

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/lotka_volterra_competition_ch/) |
[Español](https://ecoevoapps.shinyapps.io/lotka_volterra_competition_es/) |
[English](https://ecoevoapps.shinyapps.io/lotka_volterra_competition/) |
[português](https://ecoevoapps.shinyapps.io/lotka_volterra_competition_pt) |
[Turkish](https://ecoevoapps.shinyapps.io/lotka_volterra_competition_tr/)
