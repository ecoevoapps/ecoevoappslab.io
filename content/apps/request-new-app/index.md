---
title: Request new app
summary: Don't see an app you were looking for?
authors:
- You?
tags:
- Request
date:
show_date: false
weight: 1000
# 10 and onwards for package apps
# 500 for non-package apps
# 1000 for request

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

If you would like to request an app, please make a [new issue on our GitLab repository](https://gitlab.com/ecoevoapps/ecoevoapps.gitlab.io/-/issues). Please be as specific as possible in your request so we are sure the type of model you are requesting, and let us know if you have any R code that implements the model/analysis you are requesting (but it's not required!). If you have trouble with GitLab, feel free to [email us {{< icon name="envelope" pack="fas">}}](mailto:ecoevoapps@gmail.com).

Learn more about contributing to this project [here](/contribute).
