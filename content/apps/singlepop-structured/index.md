---
title: Structured population growth
summary: Dynamics of a stage (or age) structured population
authors:
- Marcel Caritá Vaz
- Mayda Nathan
tags:
- Population ecology
date: 2020-07-19T22:15:11-07:00
show_date: false
weight: 30

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics of a single **structured** population (i.e. a population comprised of individuals of different ages or different stages, which can differ in their life history traits).

**Launch app in**:
[中文](https://ecoevoapps.shinyapps.io/structured_population_dynamics_ch/) |
[Español](https://ecoevoapps.shinyapps.io/structured_population_dynamics_es/) |
[English](https://ecoevoapps.shinyapps.io/structured_population_dynamics/) |
[português](https://ecoevoapps.shinyapps.io/structured_population_dynamics_pt/) |
[Turkish](https://ecoevoapps.shinyapps.io/structured_population_dynamics_tr/)

**Other apps**:

- An alternative app that allows users to define parameters of a Leslie matrix to simulate population dynamics: [English](https://gauravsk.shinyapps.io/stage-structured-growth/)
  > *Thanks to Dan Gruner and [Mayda Nathan](/authors/mayda-nathan) for contributing this app!*

- An app that allows users to observe how random variation in demographic rates (demographic stochasticity) affects population dynamics: [English](https://gauravsk.shinyapps.io/demog-stochasticity/)
  > *Thanks to Dan Gruner and [Mayda Nathan](/authors/mayda-nathan) for contributing this app!*
