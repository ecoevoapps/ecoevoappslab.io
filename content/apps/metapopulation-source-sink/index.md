---
title: Source-sink dynamics
summary: Metapopulation dynamics from Pulliam's classic source-sink models
authors:
- Xinyi Yan
tags:
- Population ecology
- Landscape ecology
date: 2020-07-19T22:15:56-07:00
show_date: false
weight: 110

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption:
  focal_point:
---

This app simulates the dynamics from [Pulliam (1988)](https://www.researchgate.net/publication/261174651_Sources_sinks_and_population_regulation), which considers the dynamics of a population that grows across several patches. The population can maintain a positive growth rate in some patches -- called sources -- but individuals of this population may also be found in habitats of poor quality where they can't maintain a positive growth rate -- called sinks.

**Launch app in**:
[English](https://ecoevoapps.shinyapps.io/source-sink-dynamics/)
