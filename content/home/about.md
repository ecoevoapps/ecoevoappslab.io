---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 10

design:
  columns: "1"
  spacing:
    padding: ["20px", "0", "20px", "0"] # top, right, bottom, left
---

### EcoEvoApps is a collection of freely available apps that allow users to interactively explore ecology and evolution models

We are building this website as a community resource for educators and students looking to supplement their teaching and learning of these models, and for researchers looking to build and share new apps! This project is still under development, and we'd love to [hear from you {{< icon name="envelope" pack="fas">}}](mailto:ecoevoapps@gmail.com) with any suggestions for improvements or requests for new apps.

**Here's how it works**: Users can set parameter values, and R code runs in the background to update the model ouputs.

{{< figure src="demos/logistic.gif" caption="Logistic growth model in the [continuous population dynamics app](/apps/singlepop-continuous)" alt="Logistic growth app demo" >}}
