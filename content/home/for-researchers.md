---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

design:
  columns: "1"
  spacing:
    padding: ["20px", "0", "20px", "0"] # top, right, bottom, left
---

### For researchers

Interactive apps can be a great way to learn/review existing models, or to share your work with a wide audience of colleagues, students, and other members of the broader public. We invite you to [explore our existing models](/app-collection) and to [contribute new models](/contribute/#contribute-a-new-app) to EcoEvoApps!

**[Join our mailing list](/contribute/#join-our-mailing-list)** to join the EcoEvoApps community and receive updates on the project.
