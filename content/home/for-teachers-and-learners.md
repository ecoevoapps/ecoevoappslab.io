---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 20

design:
  columns: "1"
  spacing:
    padding: ["20px", "0", "20px", "0"] # top, right, bottom, left
---

### For teachers and learners

Interactive apps can be a great way to explore how changes in parameter values can change the dynamics of a model. We intend for EcoEvoApps to be used as a supplement to (rather than as a replacement for) other ways of learning these models, e.g. analytically solving for equilibria. Start exploring the [models](/app-collection), and find out how to [request new apps](/contribute/#request-a-new-app) or give us feedback on existing apps!

**[Join our mailing list](/contribute/#join-our-mailing-list)** to join the EcoEvoApps community and receive updates on the project.

{{< figure src="demos/mac-rosen.gif" caption="MacArthur-Rosenzweig model in the [predator-prey dynamics app](apps/predator-prey-dynamics)" alt="MacArthur-Rosenzweig app demo" >}}
