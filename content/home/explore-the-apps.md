---
# An instance of the About widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: hero

# Activate this widget? true/false
active: true

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `cta`.
#   Remove a link/note by deleting a cta/note block.
cta:
  url: '/app-collection'
  label: Explore the apps!
  # icon_pack: fas
  # icon: rocket
# cta_alt:
#   url: '/app-collection'
#   label: Explore the apps!

# Note. An optional note to show underneath the links.
cta_note:
  label: ''

design:
  columns: "1"
  spacing:
    padding: ["80px", "0", "0", "0"] # top, right, bottom, left
---
