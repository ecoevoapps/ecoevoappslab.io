---
# Display name
title: "Kenji Hayashi"

# Username (this should match the folder name and the name on publications)
authors:
- "kenji-hayashi"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
pronouns: He/Him/His

# Organizations/Affiliations
organizations:
- name: 
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

# List each interest with a dash
interests:
  - Community ecology
  - Biogeography
  - Cooking
  - Tennis

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:kthayashi@ucla.edu  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/kthayashi
# personal website
- icon: globe
  icon_pack: fas
  link: https://kthayashi.github.io
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers
weight: 2
---

Kenji is a PhD student at UCLA with broad interests in community ecology and biogeography. His current research leverages experimental demographic data to understand the abiotic and biotic determinants of plant species' distributions across spatial scales. He is passionate about plants, R, and evidence-based STEM teaching. In his free time, he enjoys cooking and playing tennis

Link to Kenji's personal website: https://kthayashi.github.io/
