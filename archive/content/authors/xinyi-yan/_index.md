---
# Display name
title: Xinyi Yan

# Username (this should match the folder name and the name on publications)
authors:
- "xinyi-yan"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role:

# Organizations/Affiliations
organizations:
- name: 
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

# List each interest with a dash
interests:
- Plant diversity
- Mathematics & theoretical ecology
- Spending time in nature
- Painting

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
#- icon: envelope
#  icon_pack: fas
#  link: 'mailto:test@example.org'  # For a direct email link, use "mailto:test@example.org".
- icon: facebook
  icon_pack: fab
  link: https://www.facebook.com/alex.yan.9809672
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=tDrFRxgAAAAJ&hl=en&oi=ao
  
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers
- App translators
weight: 4
---

Xinyi recently graduated from UCLA with a B.S. in Applied Math and Biology. Currently she is starting her PhD program in EEB at UT Austin, co-advised by Dr. Farrior and Dr. Wolf. She is interested in studying the influence of soil microbes in plant interactions and diversity. 

Xinyi had a childhood dream of becoming a curator of a botanical garden, which inspired her to study plants and ecology. She enjoys art and painting, where science (Escher is among her most admired artists) and nature happen to also be her beloved themes. 
