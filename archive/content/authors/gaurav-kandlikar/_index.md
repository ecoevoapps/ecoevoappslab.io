---
# Display name
title: "Gaurav Kandlikar"

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
pronouns: He/Him/His

# Organizations/Affiliations
organizations:
- name: 
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- Species coexistence
- Plant diversity
- Running

#education:
#  courses:
#  - course: 
#    institution: 
#    year: 


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:gaurav.kandlikar@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gauravsk_
- icon: globe
  icon_pack: fas
  link: https://gauravsk.gitlab.io

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Developers
weight: 1
---

Gaurav Kandlikar is a postdoc at the University of Missouri, where his research has focuses on how soil microorganisms and abiotic heterogeneity shape plant species coexistence. He became fascinated with mathematical ecology during the final year of his undergraduate, when he realized that he was learning how the same mathematical framework can be used to study predator-prey models in an ecology class (functional responses with handling time), and enzyme kinetics in a biochemistry class (Michelis-Menten dynamics). 

Gaurav's love for ecology has roots in his childhood in southern India, which he spent scrambling up trees and watching lots of Animal Planet. He's always happy to talk about ecological models, diversity in EEB, pedagogy, running, and cooking. 

For more about Gaurav, check out [his website](https://gauravsk.gitlab.io)
