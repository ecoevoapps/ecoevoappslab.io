---
# Display name
title: "Lauren Sullivan"

# Username (this should match the folder name)
authors:
- lauren-sullivan

# Is this the primary user of the site?
superuser: true

# Role/position
pronouns: She/Her/Hers

# Organizations/Affiliations
organizations:
- name: 
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: 

interests:
- movement ecology
- restoration ecology
- reading fiction that teaches her how to be a better person

#education:
#  courses:
#  - course: 
#    institution: 
#    year: 


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sullivanll@missouri.edu'  # For a direct email link, use "mailto:test@example.org".
- icon: globe
  icon_pack: fas
  link: https://laurenlsullivan.weebly.com/

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
 - Instructors
weight: 6
---

Lauren Sullivan is an assistant professor at the University of Missouri where she and [her lab](https://laurenlsullivan.weebly.com/) study how plants move and the consequences of this movement for population and community dynamics.  They also love trying to figure out how to create more diverse prairie restorations.

Lauren turned from nature-phobic to nature-philic once she took a class on the spring flora of Northern Michigan and hasn't looked back since.  Plants are so amazing that they changed her whole life!
