+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["40px", "0", "20px", "0"]

+++

### EcoEvoApps is a collection of freely available apps that allow users to interactively explore ecology and evolution models

We are building this website as a community resource for educators and students looking to supplement their learning of these models, and for researchers looking to build and share new apps! This project is still under development, and we'd love to [hear from you](mailto:ecoevoapps@gmail.com) with any suggestions for improvements or requests for new apps.

**Here's how it works**: users can set parameter values, and `R` code runs in the background to update the model. 
{{< figure library="true" src="logistic.gif" aria-label="logistic growth image" title="Logistic growth in the [continuous population dynamics app](project/singlepop-continuous)" >}}
