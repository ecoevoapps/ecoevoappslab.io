+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 25  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"


  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 # css_style = "{color: #b67d7d; font-size:12px}"
 css_style = " "
 # CSS class.
 css_class = ""
+++

### For teachers and learners

Interactive apps can be a great way to explore how changes in parameter values can change the dynamics of a model. We intend for EcoEvoApps to be used as a supplement to (rather than as a replacement for) other ways of learning these models, e.g. analytically solving for equilibria. Start exploring the [introductory models](#projects), and find out how to [request new apps]({{< ref "/contribute.md" >}}) or give us feedback on existing apps! 

You can also **[join our mailing list](/contribute/#join-our-mailing-list)** to join the EcoEvoApps community and receive updates on the project.

