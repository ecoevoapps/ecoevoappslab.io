---
title: Land Acknowledgement
summary:
date: "2018-06-28T00:00:00Z"
---

This website was established by graduate students in the UCLA EEB Department. We acknowledge the Gabrielino/Tongva peoples as the traditional land caretakers of [Tovaangar](https://www.aisc.ucla.edu/sounds/world.wav) (the Los Angeles basin and So. Channel Islands). As a land grant institution, we pay our respects to the [Honuukvetam](https://www.aisc.ucla.edu/sounds/ancestors.wav) (Ancestors), [‘Ahiihirom](https://www.aisc.ucla.edu/sounds/wise%20people.wav) (Elders) and [‘Eyoohiinkem](https://www.aisc.ucla.edu/sounds/our%20relatives.wav) (our relatives/relations) past, present and emerging.


