+++
# A "Meet the Team" section created with the People widget.
# This section displays people from `content/authors/` which belong to the `user_groups` below.

widget = "people"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = false  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 68  # Order that this section will appear.

title = "### Meet the team"
subtitle = ""

[background]
  # Background color.
  # color = "#fffff8"
  
[content]
  # Choose which groups/teams of users to display.
  #   Edit `user_groups` in each user's profile to add them to one or more of these groups.
  user_groups = ["Developers", "App contributors", "Instructors", "App translators", "App reviewers"]

[design]
  # Show user's social networking links? (true/false)
  show_social = false

  # Show user's interests? (true/false)
  show_interests = false


  # Text color (true=light or false=dark).
  # text_color_light = true  

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["40px", "0", "47px", "0"]
  
[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

<style>
h4{font-weight: normal;}
</style>

This project was initiated by a group of ecology and evolutionary biology graduate students who started writing these apps to help understand the models for ourselves, and as a teaching tools for our students. None of us are experts in R, Shiny, web development, or even the models themselves, but we've learned a lot (and had a lot of fun) along the way. We are happy to share what we've learned, and eagerly welcome contributions from anyone who wants to [contribute]({{< ref "/contribute.md" >}}) to this effort! **We actively welcome from people of all backgrounds to join us.**

All contributors must adhere to our [Contributors Code of Conduct]({{< ref "/codeOfConduct.md" >}}).

