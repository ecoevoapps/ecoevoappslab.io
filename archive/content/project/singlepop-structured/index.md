---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Structured population growth"
summary: "Dynamics of a stage (or age) structured population"
authors: ["Marcel Vaz", "Mayda Nathan"]
tags: ["Population ecology"]
categories: []
date: 2020-07-19T22:15:11-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

This app simulates the dynamics of a single **structured** population (i.e. a population comprised of individuals of different ages or different stages, which can differ in their life history traits). 

**Link to app**: https://ecoevoapps.shinyapps.io/structured_population/

*Links to other apps that also simulate the dynamics of stage-structured populations*:  
- An alternative app that allows users to define parameters of a Leslie matrix to simulate population dynamics: https://gauravsk.shinyapps.io/stage-structured-growth/. Thanks to Dan Gruner and Mayda Nathan for contributing this app!

- An app that allows users to observe how random variation in demographic rates (demographic stochasticity) affects population dynamics: https://gauravsk.shinyapps.io/demog-stochasticity/.  Thanks to Dan Gruner and Mayda Nathan for contributing this app!
