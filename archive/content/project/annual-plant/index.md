---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Annual plant competition"
summary: "Beverton-Holt model for competing annual plants"
authors: ["Gaurav Kandlikar"]
maintainers: ["Gaurav Kandlikar"]
tags: ["Species interactions"]
categories: []
date: 2020-07-03T08:27:58-07:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  captioen: ""
  focal_point: ""
  preview_only: false
---

This app implements the Beverton-Holt competition model, which describes the dynamics of competing annual plant species in the field. Niche and fitness differences between the competing plant species are computed based on the formulae developed in Chesson ([2012](https://doi.org/10.1007/978-1-4614-5755-8_13)) and Godoy and Levine ([2014](https://doi.org/10.1890/13-1157.1)). 

**Link to app**: https://gauravsk.shinyapps.io/annual-plant
