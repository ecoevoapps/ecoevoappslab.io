---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Single population growth in continuous time"
summary: "Exponential or logistic growth"
authors: ["Gaurav Kandlikar"]
tags: ["Population ecology", "basic"]
categories: []
date: 2020-07-19T22:14:56-07:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

This app simulates the dynamics of a single population growing in continuous time. The growth may be exponential (density independent), or logistically (density dependent) until a user-determined carrying capacity. If population growth is logistic, the app also allows there to be a time-lag in the density dependence. 

**Link to app**: https://ecoevoapps.shinyapps.io/single-population-continuous/


