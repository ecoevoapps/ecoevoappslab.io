---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Request New App"
summary: "Don't see an app you were looking for?"
authors: [" "]
tags: []
categories: []
date: 2000-07-21T16:19:22-07:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

If you would like to request an app, please make a [new issue on our Gitlab repository](https://gitlab.com/ecoevoapps/ecoevoapps.gitlab.io/-/issues). Please be as specific as possible in your request so we are sure the type of model you are requesting, and let us know if you have any `R` code that implements the model/analysis you are requesting (but it's not required!). 

If you have trouble with Gitlab, feel free to [email us]().
