---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Consumer-resource dynamics"
summary: "Dynamics of a predator (or herbivore) and its prey"
authors: ["Maddi Cowen", "Rosa McGuire"]
tags: ["Species interactions", "basic"]
categories: []
date: 2020-07-19T22:15:41-07:00


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---

This app implements a range of models that simulate the dynamics of a predator (or hebivore) and their prey. Users can explore a range of options, including prey that grow exponentially or logistically, and predators that have type 1 or type 2 functional responses. (i.e. Lotka-Volterra predator-prey dynamics; MacArthur-Rosenzweig dynamics, etc.)

**Link to app**: https://ecoevoapps.shinyapps.io/consumer-resource-dynamics/



