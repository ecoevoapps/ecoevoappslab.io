---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Island Biogeography"
summary: "MacArthur and Wilson's model of island biogeography"
authors: ["Marcel Vaz"]
tags: ["Landscape ecology", "basic"]
categories: []
date: 2020-07-19T22:15:47-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

This app simulates the dynamics of the classic island biogeography framework developed by [MacArthur and Wilson](https://en.wikipedia.org/wiki/Insular_biogeography). Users can set the distance between mainland and island, island size, and species richness of the mainland.  

**Links to app**: 

[中文](https://ecoevoapps.shinyapps.io/island_biogeography_ch)| [Español](https://ecoevoapps.shinyapps.io/island_biogeography_es) | [English](https://ecoevoapps.shinyapps.io/island_biogeography) |  [português](https://ecoevoapps.shinyapps.io/island_biogeography_pt) |  [Turkish](https://ecoevoapps.shinyapps.io/island_biogeography_tr)

