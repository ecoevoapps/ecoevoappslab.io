---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Plant-soil feedbacks"
summary: "Soil microbes and resource competition jointly influence plant coexistence"
authors: ["Gaurav Kandlikar"]
tags: ["Species interactions"]
categories: []
date: 2020-07-03T08:27:58-07:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  captioen: ""
  focal_point: ""
  preview_only: false
---

This Shiny app is an interactive for the plant-soil feedback model developed in Kandlikar et al.  [2019](http://doi.org/c6d4). The app includes three models. The first model implements dynamics of a system with two plant species interacting via soil microbes and via (phenomenological) competition. The second model builds on the first by simulating dynamics of a *three* plant species system with plant-soil feedbacks and phenomenological competition. THe final model explores the effects of the soil microbial community cultivated by two plant species that are explicitly competing for a dynamic resource.


**Link to app**: https://gauravsk.shinyapps.io/microbe-mediated-fitdiffs/
