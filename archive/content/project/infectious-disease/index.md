---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Infectious Disease"
summary: "Compartment models of disease dynamics"
authors: ["Maddi Cowen"]
tags: ["Species interactions"]
categories: []
date: 2020-07-19T22:16:15-07:00


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---

This app simulates the dynamics of a class of models that follow of an infectious disease spreading through a population that consists of individuals who are susceptible, infectious, recovered, and in some cases immune ("[compartment models](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology)").

**Link to app**: https://ecoevoapps.shinyapps.io/infectious-disease-dynamics/



