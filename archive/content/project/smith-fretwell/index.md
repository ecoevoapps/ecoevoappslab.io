---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Seed size/number tradeoff"
summary: "A model to explore the life history tradeoff between offspring size and offspring number"
authors: ["Gaurav Kandlikar"]
tags: ["Population ecology"]
categories: []
date: 2021-05-13T22:15:11-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

This app simulates the allocation trade-off model from [Smith and Fretwell](https://www.journals.uchicago.edu/doi/10.1086/282929)'s 1974 paper. This model explores the fundamental life history trade-off from the perspective of a tree that can invest a set amount of resources into making seeds of varying sizes. It can make many small seeds with low survival rate per seed, or fewer large seeds that are more likely to survive. 

**Link to app**:  https://ecoevoapps.shinyapps.io/smith-fretwell-app/

