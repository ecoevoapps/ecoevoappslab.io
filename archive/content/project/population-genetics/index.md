---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Population genetics"
summary: "A collection of apps for teaching quantitative genetics models"
authors: ["Chris Muir"]
tags: ["Population genetics"]
categories: []
date: 2021-04-19T22:15:26-07:00

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

*Thanks to Dr. Chris Muir for contributing these apps to EcoEvoApps!*

This is a series of apps that explore different ideas from population genetics. The first app illustrates how allele frequency changes over time due to genetic drift, and how population size affects this process.  The second app incorporates the effect of natural selection, which acts on loci with differing levels of dominance. The third app illustrates how allele copies that are present in the population in the present coalesce back through time to a common ancestor.  The final app illustrates how mutations accumulate in the along genes in haploid species, and how this influences the phylogenetic inferences that are drawn from those sequences.

**Link to apps**:
- Genetic drift and allele frequency: https://cdmuir.shinyapps.io/Genetic-Drift-Fixation/
- Viability selection: https://cdmuir.shinyapps.io/Viability-selection1/
- Gene tree and coalescence: https://cdmuir.shinyapps.io/coalescence/
- Phlygenetic inference and gene trees: https://cdmuir.shinyapps.io/genetree-to-phylogeny/
