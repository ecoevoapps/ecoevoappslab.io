---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Single population growth in discrete time"
summary: "Exponential or logistic growth"
authors: ["Gaurav Kandlikar", "Alejandra Martínez Blancas", "Xinyi Yan"]
tags: ["Population ecology"]
categories: []
date: 2020-07-19T22:15:01-07:00


# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

---


This app simulates the dynamics of a single population growing in discrete time (e.g. annually). The growth may be exponential (density independent), or logistically (density dependent) until a user-determined carrying capacity. The app implements three different models of density-dependent discrete-time growth, including [Beverton-Holt](https://en.wikipedia.org/wiki/Beverton-Holt_model) and [Ricker Models](https://en.wikipedia.org/wiki/Ricker_model).

**Link to app**: https://ecoevoapps.shinyapps.io/single-population-discrete/


