# EcoEvoApps Website

Thanks for visiting! This is the GitLab repository for the [EcoEvoApps website](https://ecoevoapps.gitlab.io). This website is built using the [Academic Theme](https://github.com/wowchemy/starter-hugo-academic) for [Hugo](https://gohugo.io/) by [Wowchemy](https://wowchemy.com/).

The source code for the `ecoevoapps` R package is available [here](https://gitlab.com/ecoevoapps/ecoevoapps).

To contribute to this project, please check out [our website](https://ecoevoapps.gitlab.io/contribute/)!
